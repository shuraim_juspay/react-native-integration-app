## React Native App Integration with Juspay SDK V1/V2

### Initial Setup

- Setup React Native with React Native CLI ( Refer [https://facebook.github.io/react-native/docs/getting-started](https://facebook.github.io/react-native/docs/getting-started))

- Install ADB

- Install Android SDK

### Project Setup

- Install dependencies by running "npm i" in the root directory

- Navigate to "android" folder, create a file named "local.properties" and include the Android SDK location. Refer the following snippet

`sdk.dir = /Users/juspay/Library/Android/sdk`

- Run "react-native run-android" to start a metro server (Runs on port 8081) and install the app on the test device (Make sure device is connected)

### Project Anatomy

- Android maven and gradle dependencies can be exposed by navigating to "build.gradle", or open the "android" folder in Android Studio

- Payload constants can exposed by navigating to "android/app/java/com.juspaypayment/Payload.java"

- Internal methods for calling Juspay SDK can exposed by navigating to "android/app/java/com.juspaypayment/PaymentModule.java"

- Front-end react native UI can be exposed by navigating to "App.js" in the root directory
