package com.juspaypayment;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

public class Payload {

    public static String getTimeStamp() {
        return Long.toString(System.currentTimeMillis());
    }

    public static JSONObject generateSignaturePayload() {
        JSONObject signaturePayload = new JSONObject();
        try {
            signaturePayload.put("first_name", PayloadConstants.firstName);
            signaturePayload.put("last_name", PayloadConstants.lastName);
            signaturePayload.put("mobile_number", PayloadConstants.mobileNumber);
            signaturePayload.put("email_address", PayloadConstants.emailAddress);
            signaturePayload.put("customer_id", PayloadConstants.customerId);
            signaturePayload.put("timestamp", getTimeStamp());
            signaturePayload.put("merchant_id", PayloadConstants.merchantId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return signaturePayload;
    }

    public static JSONObject generateInitiatePayload(JSONObject signaturePayload, String signature) {
        JSONObject initiatePayload = new JSONObject();
        try {
            initiatePayload.put("action", PayloadConstants.init_action);
            initiatePayload.put("clientId", PayloadConstants.clientId);
            initiatePayload.put("merchantKeyId", PayloadConstants.merchantKeyId);
            initiatePayload.put("signaturePayload", signaturePayload.toString());
            initiatePayload.put("signature", signature);
            initiatePayload.put("environment", PayloadConstants.environment);
            initiatePayload.put("betaAssets", PayloadConstants.betaAssets);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return initiatePayload;
    }

    public static JSONObject generateInitiatePayloadV2(String clientAuthToken) {
        JSONObject initiatePayload = new JSONObject();
        try {
            initiatePayload.put("action", PayloadConstants.init_action);
            initiatePayload.put("clientId", PayloadConstants.clientId);
            initiatePayload.put("clientAuthToken", clientAuthToken);
            initiatePayload.put("merchantId", PayloadConstants.merchantId);
            initiatePayload.put("customerId", PayloadConstants.mobileNumber);
            initiatePayload.put("mobileNumber", PayloadConstants.mobileNumber);
            initiatePayload.put("emailAddress", PayloadConstants.emailAddress);
            initiatePayload.put("environment", PayloadConstants.environment);
            initiatePayload.put("betaAssets", PayloadConstants.betaAssets);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return initiatePayload;
    }

    public static JSONObject generateOrderDetails(String orderId) {
        JSONObject orderDetails = new JSONObject();
        try {
            orderDetails.put("order_id", orderId);
            orderDetails.put("customer_phone", PayloadConstants.mobileNumber);
            orderDetails.put("customer_email", PayloadConstants.emailAddress);
            orderDetails.put("customer_id", PayloadConstants.customerId);
            orderDetails.put("timestamp", getTimeStamp());
            orderDetails.put("merchant_id", PayloadConstants.merchantId);
            orderDetails.put("amount", PayloadConstants.amount);
            orderDetails.put("return_url", PayloadConstants.returnUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return orderDetails;
    }

    public static JSONObject generateProcessPayload(String orderId, JSONObject orderDetails, String signature) {
        JSONObject processPayload = new JSONObject();
        try {
            processPayload.put("action", PayloadConstants.process_action);
            processPayload.put("merchantId", PayloadConstants.merchantId);
            processPayload.put("clientId", PayloadConstants.clientId);
            processPayload.put("orderId", orderId);
            processPayload.put("amount", PayloadConstants.amount);
            processPayload.put("customerId", PayloadConstants.customerId);
            processPayload.put("customerEmail", PayloadConstants.emailAddress);
            processPayload.put("customerMobile", PayloadConstants.mobileNumber);

            ArrayList<String> endUrlArr = new ArrayList<>(Arrays.asList(".*sandbox.juspay.in\\/thankyou.*", ".*sandbox.juspay.in\\/end.*", ".*localhost.*", ".*api.juspay.in\\/end.*"));

            JSONArray endUrls = new JSONArray(endUrlArr);

            processPayload.put("endUrls", endUrls);

            processPayload.put("merchantKeyId", PayloadConstants.merchantKeyId);
            processPayload.put("orderDetails", orderDetails.toString());
            processPayload.put("signature", signature);
            processPayload.put("environment", PayloadConstants.environment);
            processPayload.put("language", PayloadConstants.language);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processPayload;
    }

    public static JSONObject generateProcessPayloadV2(String orderId, String clientAuthToken) {
        JSONObject processPayload = new JSONObject();
        try {
            processPayload.put("action", PayloadConstants.process_action);
            processPayload.put("merchantId", PayloadConstants.merchantId);
            processPayload.put("clientId", PayloadConstants.clientId);
            processPayload.put("orderId", orderId);
            processPayload.put("amount", PayloadConstants.amount);
            processPayload.put("customerId", PayloadConstants.mobileNumber);
            processPayload.put("customerEmail", PayloadConstants.emailAddress);
            processPayload.put("customerMobile", PayloadConstants.mobileNumber);

            ArrayList<String> endUrlArr = new ArrayList<>(Arrays.asList(".*sandbox.juspay.in\\/thankyou.*", ".*sandbox.juspay.in\\/end.*", ".*localhost.*", ".*api.juspay.in\\/end.*"));
            JSONArray endUrls = new JSONArray(endUrlArr);

            processPayload.put("endUrls", endUrls);

            processPayload.put("clientAuthToken", clientAuthToken);

            processPayload.put("language", PayloadConstants.language);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processPayload;
    }

    public static JSONObject getPaymentsPayload(String requestId, JSONObject payload) {
        JSONObject paymentsPayload = new JSONObject();
        try {
            paymentsPayload.put("requestId", requestId);
            paymentsPayload.put("service", PayloadConstants.service);
            paymentsPayload.put("payload", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return paymentsPayload;
    }

    public static String generateRequestId() {
        String[] uuid = UUID.randomUUID().toString().split("-");
        for (int i = 0; i < uuid.length; i++) {
            if (i % 2 != 0) {
                uuid[i] = uuid[i].toUpperCase();
            }
        }
        return TextUtils.join("-", uuid);
    }

    public static JSONObject generateProcessJson(String requestId, JSONObject payload) {
        JSONObject paymentsPayload = new JSONObject();
        try {
            paymentsPayload.put("requestId", requestId);
            paymentsPayload.put("service", PayloadConstants.service);
            paymentsPayload.put("payload", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return paymentsPayload;
    }

    public static String generateOrderId() {
        return "R" + (long) (Math.random() * 10000000000L);
    }

    public static JSONObject generateOrderJSON(String orderId) {
        JSONObject obj = new JSONObject();

        try {
            obj.put("order_id", orderId);
            obj.put("amount", PayloadConstants.amount);
            obj.put("customer_id", PayloadConstants.mobileNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public abstract class PayloadConstants {

        final public static String service = "in.juspay.hyperpay";

        final public static String apiKey = "F43126254264C2DBF9999FF7333B6E";
        final public static String mobileNumber = "9876543210";
        final public static String clientId = "hyper_beta_android";
        final public static String firstName = "Test";
        final public static String lastName = "User";
        final public static String emailAddress = "test@juspay.in";
        final public static String customerId = "9876543210";
        final public static String merchantId = "hyper_beta";

        final public static String init_action = "initiate";
        final public static String process_action = "payment_page";
        final public static String merchantKeyId = "2992";
        final public static String environment = "sandbox";

        final public static String amount = "1.0";
        final public static String returnUrl = "https://sandbox.juspay.in/end";
        final public static String language = "english";

        final public static String signatureURL = "https://dry-cliffs-89916.herokuapp.com/sign-hyper-beta?payload=";

        final public static boolean betaAssets = true;

    }
}
