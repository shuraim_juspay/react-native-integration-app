
package com.juspaypayment;

import android.os.AsyncTask;
import android.telecom.Call;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;


import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import com.facebook.react.bridge.Callback;
import com.facebook.react.uimanager.IllegalViewOperationException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;

import in.juspay.godel.PaymentActivity;
import in.juspay.godel.data.JuspayResponseHandler;
import in.juspay.godel.ui.HyperPaymentsCallbackAdapter;
import in.juspay.services.PaymentServices;

public class PaymentModule extends ReactContextBaseJavaModule {
  public boolean isPrefetch = false;

  public JSONObject signaturePayload;
  public JSONObject initiatePayload;

  public String initiateSignature;

  public boolean isSignaturePayloadSigned;
  public boolean isInitiateDone;
  public JSONObject initiateResult;

  public JSONObject initiateJson;
  public JSONObject processJson;


  // Variables for process
  public JSONObject orderDetails;
  public JSONObject processPayload;


  public String orderId;
  public String processSignature;

  public boolean isOrderIDGenerated;
  public boolean isOrderDetailsSigned;
  public boolean isProcessDone;
  public JSONObject processResult;


  // Payment services
  public PaymentServices juspayServices;
  public String requestId;

  public String clientAuthToken;
  public boolean isClientAuthTokenGenerated;

  public static PaymentModule instance = null;


  PaymentModule(ReactApplicationContext context) {
    super(context);
  }

  @Override
  public String getName() {
    return "PaymentModule";
  }

  @ReactMethod
  public void generateToast(String str) {
      Toast.makeText(getReactApplicationContext(), str, Toast.LENGTH_SHORT).show();
  }

  @ReactMethod
  public void showPrefetchInput(Callback callback){
    try{
      callback.invoke(null, "clientId = "+ Payload.PayloadConstants.clientId+"\nbooleanAssets = "+ Payload.PayloadConstants.betaAssets);
    }catch (Exception e){
      callback.invoke(e.toString(), null);
    }
  }

  @ReactMethod
  public void preFetch(){
    isPrefetch = true;

    generateSignaturePayload();

    juspayServices = new PaymentServices(MainActivity.instance);

    isSignaturePayloadSigned = false;
    isOrderDetailsSigned = false;
    isInitiateDone = false;
    isProcessDone = false;

    isOrderIDGenerated = false;
    orderId = "";

    isClientAuthTokenGenerated = false;

    requestId = Payload.generateRequestId();
    initiateSignature = "";
    initiateResult = new JSONObject();
    processSignature = "";
    processResult = new JSONObject();
    PaymentActivity.preFetch(MainActivity.instance, Payload.PayloadConstants.clientId, Payload.PayloadConstants.betaAssets);
    Toast.makeText(getReactApplicationContext(), "Prefetched!", Toast.LENGTH_SHORT).show();
  }

  @ReactMethod
  public void clearVariables() {
    signaturePayload = new JSONObject();
    initiatePayload = new JSONObject();

    initiateSignature = "";

    isSignaturePayloadSigned = false;
    isInitiateDone = false;
    initiateResult = new JSONObject();

    initiateJson = new JSONObject();
    processJson = new JSONObject();


    // Variables for process
    orderDetails = new JSONObject();
    processPayload = new JSONObject();


    orderId = "";
    processSignature = "";

    isOrderIDGenerated = false;
    isOrderDetailsSigned = false;
    isProcessDone = false;
    processResult = new JSONObject();

    requestId = "";

    clientAuthToken = "";
    isClientAuthTokenGenerated = false;
  }

  @ReactMethod
  public void showSignatureInput(Callback callback) {
    try{
      callback.invoke(null, signaturePayload.toString());
    }catch (Exception e){
      callback.invoke(e.toString(), null);
    }
  }

  @ReactMethod
  public void showSignatureOutput(Callback callback) {
    try{
      callback.invoke(null, initiateSignature.toString());
    }catch (Exception e){
      callback.invoke(e.toString(), null);
    }
  }

  @ReactMethod
  public void showInitiateInput(Callback callback) {
    try{
      if (isSignaturePayloadSigned) {
        JSONObject payload = Payload.getPaymentsPayload(requestId, initiatePayload);
        callback.invoke(null, payload.toString());
      } else {
        Toast.makeText(getReactApplicationContext(), "Sign payload first", Toast.LENGTH_SHORT).show();
      }
    }catch (Exception e){
      callback.invoke(e.toString(), null);
    }
  }

  @ReactMethod
  public void showClientAuthTokenInput(Callback callback) {
    try{
        callback.invoke(null, "apiKey: "+Payload.PayloadConstants.apiKey);
    }catch (Exception e){
      callback.invoke(e.toString(), null);
    }
  }

  @ReactMethod
  public void showClientAuthTokenOutput(Callback callback) {
    try{
      if(isClientAuthTokenGenerated)
        callback.invoke(null, clientAuthToken);
      else
        Toast.makeText(getReactApplicationContext(), "Generate clientAuthToken to see output", Toast.LENGTH_SHORT).show();
    }catch (Exception e){
      callback.invoke(e.toString(), null);
    }
  }

  @ReactMethod
  public void showInitiateInputV2(Callback callback) {
    try{
      if (isClientAuthTokenGenerated) {
        callback.invoke(null, initiateJson.toString());
      } else {
        Toast.makeText(getReactApplicationContext(), "Generate clientAuthToken first", Toast.LENGTH_SHORT).show();
      }
    }catch (Exception e){
      callback.invoke(e.toString(), null);
    }
  }

  @ReactMethod
  public void showInitiateOutput(Callback callback) {
    if (isInitiateDone || initiateResult.length()>0) {
      callback.invoke(null, initiateResult.toString());
    } else {
      Toast.makeText(getReactApplicationContext(), "Initiate first to see output", Toast.LENGTH_SHORT).show();
    }
  }

  @ReactMethod
  public void showInitiateOutputV2(Callback callback) {
    if (isInitiateDone || initiateResult.length()>0) {
      callback.invoke(null, initiateResult.toString());
    } else {
      Toast.makeText(getReactApplicationContext(), "Initiate first to see output", Toast.LENGTH_SHORT).show();
    }
  }

  @ReactMethod
  public void showOrderDetailsInput(Callback callback) {
    if (isOrderIDGenerated) {
      callback.invoke(null, orderDetails.toString());
    } else {
      Toast.makeText(getReactApplicationContext(), "Please generate an Order ID first", Toast.LENGTH_SHORT).show();
    }
  }

  @ReactMethod
  public void showOrderDetailsOutput(Callback callback) {
    if (isOrderDetailsSigned) {
      callback.invoke(null, processSignature.toString());
    } else {
      Toast.makeText(getReactApplicationContext(), "Please sign order details to see output", Toast.LENGTH_SHORT).show();
    }
  }

  @ReactMethod
  public void showProcessInput(Callback callback) {
    if (isOrderDetailsSigned) {
      JSONObject payload = Payload.getPaymentsPayload(requestId, processPayload);
      callback.invoke(null, payload.toString());
    } else {
      Toast.makeText(getReactApplicationContext(), "Please sign order details first", Toast.LENGTH_SHORT).show();
    }
  }

  @ReactMethod
  public void showProcessInputV2(Callback callback) {
    if (isInitiateDone) {
      callback.invoke(null, processJson.toString());
    } else {
      Toast.makeText(getReactApplicationContext(), "Please initiate first", Toast.LENGTH_SHORT).show();
    }
  }

  @ReactMethod
  public void showProcessOutput(Callback callback) {
    if (isProcessDone) {
      callback.invoke(null, processResult.toString());
    } else {
      Toast.makeText(getReactApplicationContext(), "Process not complete yet", Toast.LENGTH_SHORT).show();
    }
  }

  @ReactMethod
  public void showProcessOutputV2(Callback callback) {
    if (isProcessDone) {
      callback.invoke(null, processResult.toString());
    } else {
      Toast.makeText(getReactApplicationContext(), "Process not complete yet", Toast.LENGTH_SHORT).show();
    }
  }

  public void generateSignaturePayload(){
    signaturePayload = Payload.generateSignaturePayload();
  }

  public void generateInitiatePayload(){
    initiatePayload = Payload.generateInitiatePayload(signaturePayload, initiateSignature);
  }

  public void generateOrderDetails() {
    orderDetails = Payload.generateOrderDetails(orderId);
  }

  public void generateProcessPayload() {
    processPayload = Payload.generateProcessPayload(orderId, orderDetails, processSignature);
  }

  @ReactMethod
  public void refresh(Callback successCallback, Callback errorCallback) {
    try {
      System.out.println("refresh");
      if(processResult == null) {
        successCallback.invoke("Not Started!");
      }else{
        successCallback.invoke(processResult.toString());
      }
    } catch (IllegalViewOperationException e) {
      errorCallback.invoke(e.getMessage());
      errorCallback.invoke(e.getMessage());
    }
  }

  @ReactMethod
  public void signSingaturePayload(){
    try {
      if(isPrefetch){
        SignatureAPI signatureAPI = new SignatureAPI();
        initiateSignature = signatureAPI.execute(signaturePayload.toString()).get();
        Log.d("Initiate +++ Signature", initiateSignature);
        isSignaturePayloadSigned = true;
        Toast.makeText(getReactApplicationContext(), "Payload signed", Toast.LENGTH_SHORT).show();
        generateInitiatePayload();
      }else{
        Toast.makeText(getReactApplicationContext(), "Please prefetch application.", Toast.LENGTH_SHORT).show();
      }
    } catch (ExecutionException | InterruptedException e) {
      e.printStackTrace();
    }
  }

  @ReactMethod
  public void initiateJuspaySdk(Callback callback) {
    try {
      if (isSignaturePayloadSigned) {
        JSONObject payload = Payload.getPaymentsPayload(requestId, initiatePayload);
        Log.e("initiatePayload", payload.toString());
        juspayServices.initiate(payload, new HyperPaymentsCallbackAdapter() {
          @Override
          public void onEvent(JSONObject data, JuspayResponseHandler juspayResponseHandler) {
            Log.e("initiateResponse", data.toString());
            Log.d("Inside OnEvent ", "initiate");
            try {
              String event = data.getString("event");
              switch (event) {
                case "initiate_result":
                  isInitiateDone = true;
                  initiateResult = data;
                  Toast.makeText(getReactApplicationContext(), "Initiate Complete", Toast.LENGTH_SHORT).show();
                  Log.wtf("initiate_result", data.toString());
                  callback.invoke(null, true);
                  break;
                case "process_result":
                  isProcessDone = true;
                  processResult = data;
                  Toast.makeText(getReactApplicationContext(), "Process Complete", Toast.LENGTH_SHORT).show();
                  Log.wtf("process_result", data.toString());
                  break;
                default:
                  Toast.makeText(getReactApplicationContext(), "Unknown Result", Toast.LENGTH_SHORT).show();
                  Log.wtf(event, data.toString());
                  break;
              }
            } catch (Exception e) {
              Log.d("Came here", e.toString());
              e.printStackTrace();
            }
          }
        });
      } else {
        Toast.makeText(getReactApplicationContext(), "Please sign the payload", Toast.LENGTH_SHORT).show();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @ReactMethod
  public void generateClientAuthToken() {
    new CustomerApiCaller().execute();
    Toast.makeText(getReactApplicationContext(), "clientAuthToken generated!", Toast.LENGTH_SHORT).show();
    isClientAuthTokenGenerated = true;
  }

  public void generateInitiatePayloadV2() {
    initiatePayload = Payload.generateInitiatePayloadV2(clientAuthToken);
    Log.d("init_payload", initiatePayload.toString());
  }

  public void generateInitiateJson() {
    initiateJson = Payload.getPaymentsPayload(requestId, initiatePayload);
    Log.d("init_json", initiateJson.toString());
  }

  @ReactMethod
  public void initiateJuspaySdkV2(Callback callback) {
    if(isClientAuthTokenGenerated)
    try {
      Log.e("initSending", initiateJson.toString());
      juspayServices.initiate(initiateJson, new HyperPaymentsCallbackAdapter() {
        @Override
        public void onEvent(JSONObject data, JuspayResponseHandler juspayResponseHandler) {
          try {
            String event = data.getString("event");
            Log.e("Initiate Response ", data.toString());
            switch (event) {
              case "initiate_result":
                isInitiateDone = true;
                initiateResult = data;
                Toast.makeText(getReactApplicationContext(), "Initiate Complete", Toast.LENGTH_SHORT).show();
                callback.invoke(null, true);
                break;
              case "process_result":
                isProcessDone = true;
                processResult = data;
                Toast.makeText(getReactApplicationContext(), "Process Complete", Toast.LENGTH_SHORT).show();
                break;
              default:
                initiateResult = data;
                Toast.makeText(getReactApplicationContext(), "Unknown Result", Toast.LENGTH_SHORT).show();
                break;
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      });
      generateProcessJson();
    } catch (Exception e) {
      e.printStackTrace();
    }
    else
      Toast.makeText(getReactApplicationContext(), "Please generate clientAuthToken first", Toast.LENGTH_SHORT).show();
  }



  public void generateProcessJson() {
    processJson = Payload.generateProcessJson(requestId,Payload.generateProcessPayloadV2(orderId, clientAuthToken));
  }

  @ReactMethod
  public void processJuspaySdkV2() {
    if(isOrderIDGenerated) {
      try {
        generateProcessJson();
        Log.e("Process Payload ", processJson.toString());
        juspayServices.process(processJson);
      } catch (Exception e) {
        Log.e("Exception in process", e.toString());
      }
    }
    else
      Toast.makeText(getReactApplicationContext(), "Please generate orderId first", Toast.LENGTH_SHORT).show();

  }

  @ReactMethod
  public void generateOrderID(){
    orderId = Payload.generateOrderId();
    isOrderIDGenerated = true;
    generateOrderDetails();
    Toast.makeText(getReactApplicationContext(), "OrderID generated!", Toast.LENGTH_SHORT).show();
  }

  @ReactMethod
  public void generateOrderIDV2(){
    orderId = Payload.generateOrderId();
    isOrderIDGenerated = true;
    generateOrder();
    Toast.makeText(getReactApplicationContext(), "OrderID generated!", Toast.LENGTH_SHORT).show();
  }

  public void generateOrder() {
    new OrderApiCaller().execute();
  }

  @ReactMethod
  public void signOrderDetails(){
    if (isOrderIDGenerated && isInitiateDone) {
      try {
        SignatureAPI signatureAPI = new SignatureAPI();
        processSignature = signatureAPI.execute(orderDetails.toString()).get();
        isOrderDetailsSigned = true;
        Toast.makeText(getReactApplicationContext(), "Signed Order Details", Toast.LENGTH_SHORT).show();
        generateProcessPayload();
      } catch (ExecutionException | InterruptedException e) {
        e.printStackTrace();
      }
    } else if(!isOrderIDGenerated){
      Toast.makeText(getReactApplicationContext(), "Please generate an order id", Toast.LENGTH_SHORT).show();
    }
    else
      Toast.makeText(getReactApplicationContext(), "Please initiate", Toast.LENGTH_SHORT).show();
  }

  @ReactMethod
  public void startJuspaySdk(){
    if (isOrderDetailsSigned) {
      JSONObject payload = Payload.getPaymentsPayload(requestId, processPayload);
      Log.e("processPayload", payload.toString());
      juspayServices.process(payload);
    } else {
      Toast.makeText(getReactApplicationContext(), "Please sign the payload", Toast.LENGTH_SHORT).show();
    }
  }

  @ReactMethod
  public void terminateJuspaySdk() {
    if(isPrefetch) {
      juspayServices.terminate();
      Toast.makeText(getReactApplicationContext(), "Juspay SDK terminated", Toast.LENGTH_LONG).show();

      isSignaturePayloadSigned = false;
      isInitiateDone = false;

      isClientAuthTokenGenerated = false;

      isOrderIDGenerated = false;
      isOrderDetailsSigned = false;
      isProcessDone = false;

      requestId = Payload.generateRequestId();
      processResult = new JSONObject();
    }
  }

  @ReactMethod
  public void backPressed(Callback callback){
    boolean backPressHandled = juspayServices.onBackPressed();

    try{
      callback.invoke(null, backPressHandled);
    }catch (Exception e){
      callback.invoke(e.toString(), null);
    }
  }

  public class CustomerApiCaller extends AsyncTask<URL, Integer, String> {

    protected String doInBackground(URL... urls) {

      String url = "https://sandbox.juspay.in/customers/9876543210?options.get_client_auth_token=true";

      try {
        URL httpsUrl = new URL(url);
        String auth = Payload.PayloadConstants.apiKey;

        HttpsURLConnection connection = (HttpsURLConnection)httpsUrl.openConnection();

        connection.setRequestMethod("GET");

        byte[] encodedAuth = auth.getBytes("UTF-8");

        connection.setRequestProperty("Authorization", "Basic " + new String(android.util.Base64.encode(encodedAuth, Base64.DEFAULT)));

        if(connection != null){
          Log.d("connectionRepsonse "+ connection.getResponseCode(), "");
          String resp = evaluateConnectionResponse(connection);
          Log.d("final Response", resp);


          for(int i=0; i<10; i++)
            publishProgress(i*10);

          return resp;
        }


      } catch (Exception e) {
        return e.toString();
      }
      return "error";
    }

    public String evaluateConnectionResponse(HttpsURLConnection connection){

      try {
        int responseCode = connection.getResponseCode();
        InputStreamReader responseReader;
        if ((responseCode < 200 || responseCode >= 300) && responseCode != 302) {
          responseReader = new InputStreamReader(connection.getErrorStream());
        } else {
          responseReader = new InputStreamReader(connection.getInputStream());
        }

        BufferedReader in = new BufferedReader(responseReader);
        StringBuilder response = new StringBuilder();

        String inputLine;
        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }

        in.close();
        String responsePayload = response.toString();
        connection.disconnect();

        return responsePayload;

      } catch (Exception e){
        e.printStackTrace();
      }
      return "ErrorBoy";
    }

    protected void onProgressUpdate(Integer... progress) {
      //message.setText("Done");
    }
    protected void onPostExecute(String result) {
      Log.d("onPostExecute", result);

      try {
        JSONObject res = new JSONObject(result);

        Log.e("jsonObjCust", res.toString());

        String token = res.getJSONObject("juspay").getString("client_auth_token");
        Log.d("clientTokenFromJson", token);
        clientAuthToken = token;

        generateInitiatePayloadV2();
        generateInitiateJson();
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }

  }

  public class OrderApiCaller extends AsyncTask<URL, Integer, String> {

    protected String doInBackground(URL... urls) {

      String url = "https://sandbox.juspay.in/orders/";

      try {
        URL httpsUrl = new URL(url);
        String auth = Payload.PayloadConstants.apiKey;

        HttpsURLConnection connection = (HttpsURLConnection)httpsUrl.openConnection();

        connection.setRequestMethod("POST");

        byte[] encodedAuth = auth.getBytes("UTF-8");

        JSONObject orderData = Payload.generateOrderJSON(orderId);

        connection.setRequestProperty("Authorization", "Basic " + new String(android.util.Base64.encode(encodedAuth, Base64.DEFAULT)));

        byte[] out = orderData.toString().getBytes(StandardCharsets.UTF_8);
        int length = out.length;

        connection.setFixedLengthStreamingMode(length);
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.connect();
        try(OutputStream os = connection.getOutputStream()) {
          os.write(out);
        }





        if(connection != null){
          Log.d("connectionRepsonse "+ connection.getResponseCode(), "");
          String resp = evaluateConnectionResponse(connection);
          Log.d("final Response", resp);


          for(int i=0; i<10; i++)
            publishProgress(i*10);

          return resp;
        }


      } catch (Exception e) {
        return e.toString();
      }
      return "error";
    }

    public String evaluateConnectionResponse(HttpsURLConnection connection){

      try {
        int responseCode = connection.getResponseCode();
        InputStreamReader responseReader;
        if ((responseCode < 200 || responseCode >= 300) && responseCode != 302) {
          responseReader = new InputStreamReader(connection.getErrorStream());
        } else {
          responseReader = new InputStreamReader(connection.getInputStream());
        }

        BufferedReader in = new BufferedReader(responseReader);
        StringBuilder response = new StringBuilder();

        String inputLine;
        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }

        in.close();
        String responsePayload = response.toString();
        connection.disconnect();

        return responsePayload;

      } catch (Exception e){
        e.printStackTrace();
      }
      return "ErrorBoy";
    }

    protected void onProgressUpdate(Integer... progress) {
      //message.setText("Done");
    }
    protected void onPostExecute(String result) {
      Log.d("onPostExecute", result);
    }

  }

}