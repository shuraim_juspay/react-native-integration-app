import 'react-native-gesture-handler';
import React from 'react';
import {StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Prefetch from './components/Prefetch';
import Initiate from './components/Initiate';
import InitiateV2 from './components/InitiateV2';
import Process from './components/Process';
import ProcessV2 from './components/ProcessV2';
import Version from './components/Version';

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Prefetch">
        <Stack.Screen
          name="Prefetch"
          component={Prefetch}
          options={{
            title: 'PREFETCH',
            headerStyle: styles.header,
            headerTitleStyle: styles.headerTitle,
          }}
        />
        <Stack.Screen
          name="Initiate"
          options={{
            title: 'INITIATE',
            headerStyle: styles.header,
            headerTitleStyle: styles.headerTitle,
          }}
          component={Initiate}
        />
        <Stack.Screen
          name="InitiateV2"
          options={{
            title: 'INITIATE',
            headerStyle: styles.header,
            headerTitleStyle: styles.headerTitle,
          }}
          component={InitiateV2}
        />
        <Stack.Screen
          name="Process"
          options={{
            title: 'PROCESS',
            headerStyle: styles.header,
            headerTitleStyle: styles.headerTitle,
          }}
          component={Process}
        />
        <Stack.Screen
          name="ProcessV2"
          options={{
            title: 'PROCESS',
            headerStyle: styles.header,
            headerTitleStyle: styles.headerTitle,
          }}
          component={ProcessV2}
        />
        <Stack.Screen
          name="Version"
          options={{
            title: 'VERSION',
            headerStyle: styles.header,
            headerTitleStyle: styles.headerTitle,
          }}
          component={Version}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const Stack = createStackNavigator();

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#45A4A4',
  },
  headerTitle: {
    color: '#fff',
    fontSize: 15,
    width: 100,
  },
});

export default App;
