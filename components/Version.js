import React from 'react';
import {Button, Divider} from 'react-native-elements';
import { ScrollView, StyleSheet} from 'react-native';

const Version = ({navigation}) => {
  const handleV1 = () => navigation.navigate('Initiate');

  const handleV2 = () => navigation.navigate('InitiateV2');

  return (
    <ScrollView>
      <Divider style={styles.divider} />
      <Button
        title="New Merchants / V1"
        buttonStyle={styles.button}
        onPress={handleV1}
      />
      <Divider style={styles.divider} />
      <Button
        title="Existing Merchants / V2"
        buttonStyle={styles.button}
        onPress={handleV2}
      />
      <Divider style={styles.divider} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  divider: {
    backgroundColor: '#45A4A4',
    marginTop: 30,
    marginBottom: 30,
    height: 1,
  },
  button: {
    backgroundColor: '#45A4A4',
    width: 350,
    height: 40,
    marginLeft: 20,
    marginTop: 10,
  },
});

export default Version;
