import React, {useState} from 'react';
import {Button, Divider, Text, Card, Overlay} from 'react-native-elements';
import {View, ScrollView, StyleSheet} from 'react-native';
import PaymentModule from './PaymentModule';

const Prefetch = ({navigation}) => {
  const [isOverlayVisible, setIsOverlayVisible] = useState(false);
  const [overlayContent, setOverlayContent] = useState('');

  const handleOverlay = () => setIsOverlayVisible(false);

  const handlePrefetch = () => {
    PaymentModule.preFetch();
    setIsPrefetched(true);
  };

  const handlePrefetchInput = () =>
    PaymentModule.showPrefetchInput((err, input) => {
      setOverlayContent(input);
      setIsOverlayVisible(true);
    });

  const handleProceed = () => {
    if (isPrefetched) navigation.navigate('Version');
    else PaymentModule.generateToast('Please prefetch first');
  };

  const [isPrefetched, setIsPrefetched] = useState(false);

  return (
    <ScrollView>
      <Overlay isVisible={isOverlayVisible} height="auto">
        <View>
          <Text style={{marginBottom: 50, textAlign: 'justify'}}>
            {overlayContent}
          </Text>
          <Button
            title="Close"
            buttonStyle={styles.inputButton}
            onPress={handleOverlay}
          />
        </View>
      </Overlay>
      <Divider style={styles.divider} />
      <Button
        title="INPUT"
        buttonStyle={styles.inputButton}
        onPress={handlePrefetchInput}
      />
      <Button
        title="PREFETCH"
        onPress={handlePrefetch}
        buttonStyle={styles.button}
      />
      <Divider style={styles.divider} />

      <Card style={styles.helpText}>
        <Text>
          Hit INPUT button to refer the given input given i.e paramaters passed
        </Text>
        <Text>Step 1: Hit PREFETCH button</Text>
        <Text>Step 2: Hit PROCEED button</Text>
      </Card>

      <Divider style={styles.divider} />

      <Button
        title="PROCEED"
        onPress={handleProceed}
        buttonStyle={styles.button}
      />

      <Divider style={styles.divider} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  divider: {
    backgroundColor: '#45A4A4',
    marginTop: 30,
    marginBottom: 30,
    height: 1,
  },
  inputButton: {
    backgroundColor: '#45A4A4',
    width: 150,
    height: 40,
    marginLeft: 20,
  },
  button: {
    backgroundColor: '#45A4A4',
    width: 350,
    height: 40,
    marginLeft: 20,
    marginTop: 10,
  },
  helpText: {
    color: '#45A4A4',
  },
});

export default Prefetch;
