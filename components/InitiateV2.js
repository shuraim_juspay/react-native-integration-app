import React, {useState, useEffect} from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import {Button, Divider, Text, Card, Overlay} from 'react-native-elements';
import PaymentModule from './PaymentModule';

const InitiateV2 = ({navigation}) => {

  useEffect(() => PaymentModule.clearVariables(),[]);

  const [isClienAuthToken, setIsClienAuthToken] = useState(false);
  const [isInitiated, setIsInitiated] = useState(false);

  const [isOverlayVisible, setIsOverlayVisible] = useState(false);
  const [overlayContent, setOverlayContent] = useState('');

  const handleOverlay = () => setIsOverlayVisible(false);

  const handleClienAuthTokenInput = () => {
    PaymentModule.showClientAuthTokenInput((err, input) => {
      setOverlayContent(input);
      setIsOverlayVisible(true);
    });
  };

  const handleClienAuthTokenOutput = () => {
    if (!isClienAuthToken)
      PaymentModule.generateToast(
        'Please generate clientAuthToken to see output',
      );
    else {
      PaymentModule.showClientAuthTokenOutput((err, output) => {
        setOverlayContent(output);
        setIsOverlayVisible(true);
      });
    }
  };

  const handleInitiateInput = () => {
    PaymentModule.showInitiateInputV2((err, input) => {
      setOverlayContent(input);
      setIsOverlayVisible(true);
    });
  };

  const handleInitiateOutput = () => {
    if (!isClienAuthToken)
      PaymentModule.generateToast(
        'Please generate clientAuthToken to see output',
      );
    else {
      PaymentModule.showInitiateOutputV2((err, output) => {
        setOverlayContent(output);
        setIsOverlayVisible(true);
      });
    }
  };

  const handleClientAuthToken = () => {
    PaymentModule.generateClientAuthToken();
    setIsClienAuthToken(true);
  };

  const handleInitiate = () => {
    PaymentModule.initiateJuspaySdkV2((err, isInitiateComplete) => {
      if(isInitiateComplete)
      setIsInitiated(true);
    });
  };

  const handleTerminate = () => PaymentModule.terminateJuspaySdk();

  const handleProceed = () => {
    if (!isInitiated)
      PaymentModule.generateToast('Please initiate first to proceed');
    else navigation.navigate('ProcessV2');
  };

  return (
    <ScrollView>
      <Overlay isVisible={isOverlayVisible} height="auto">
        <View>
          <Text style={{marginBottom: 50, textAlign: 'justify'}}>
            {overlayContent}
          </Text>
          <Button
            title="Close"
            buttonStyle={styles.inputButton}
            onPress={handleOverlay}
          />
        </View>
      </Overlay>
      <Divider style={styles.divider} />
      <View style={styles.inputOutputGroup}>
        <Button
          title="INPUT"
          buttonStyle={styles.inputButton}
          onPress={handleClienAuthTokenInput}
        />
        <Button
          title="OUTPUT"
          buttonStyle={styles.inputButton}
          onPress={handleClienAuthTokenOutput}
        />
      </View>
      <Button
        title="GENERATE clientAuthToken"
        buttonStyle={styles.button}
        onPress={handleClientAuthToken}
      />
      <Divider style={styles.divider} />

      <View style={styles.inputOutputGroup}>
        <Button
          title="INPUT"
          buttonStyle={styles.inputButton}
          onPress={handleInitiateInput}
        />
        <Button
          title="OUTPUT"
          buttonStyle={styles.inputButton}
          onPress={handleInitiateOutput}
        />
      </View>
      <Button
        title="INITIATE"
        buttonStyle={styles.button}
        onPress={handleInitiate}
      />
      <Divider style={styles.divider} />

      <Button
        title="TERMINATE"
        buttonStyle={styles.button}
        onPress={handleTerminate}
      />
      <Divider style={styles.divider} />

      <Card style={styles.helpText}>
        <Text>
          Hit INPUT button to refer the given input AND OUTPUT button to refer
          output
        </Text>
        <Text>Step 1: Hit GENERATE clientAuthToken button</Text>
        <Text>Step 2: Hit INITIATE button</Text>
        <Text>Step 3: Hit PROCEED button</Text>
      </Card>

      <Divider style={styles.divider} />

      <Button
        title="PROCEED"
        buttonStyle={styles.button}
        onPress={handleProceed}
      />

      <Divider style={styles.divider} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  divider: {
    backgroundColor: '#45A4A4',
    marginTop: 30,
    marginBottom: 30,
    height: 1,
  },
  inputButton: {
    backgroundColor: '#45A4A4',
    width: 150,
    height: 40,
    marginLeft: 20,
  },
  button: {
    backgroundColor: '#45A4A4',
    width: 350,
    height: 40,
    marginLeft: 20,
    marginTop: 10,
  },
  helpText: {
    color: '#45A4A4',
  },
  inputOutputGroup: {
    flexDirection: 'row',
  },
});

export default InitiateV2;
