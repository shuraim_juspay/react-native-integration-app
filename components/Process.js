import React, {useState, useEffect} from 'react';
import {View, ScrollView, StyleSheet, BackHandler} from 'react-native';
import {Button, Divider, Text, Card, Overlay} from 'react-native-elements';
import PaymentModule from './PaymentModule';

const Process = ({navigation}) => {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
  }, []);

  const handleBackButton = () => {
    PaymentModule.backPressed((err, x) => {
      console.log(x);
      if (!x) navigation.goBack();
    });

    return true;
  };

  const [isOverlayVisible, setIsOverlayVisible] = useState(false);
  const [overlayContent, setOverlayContent] = useState('');

  const [isSigned, setIsSigned] = useState(false);

  const handleOverlay = () => setIsOverlayVisible(false);

  const handleGenerateOrderId = () => PaymentModule.generateOrderID();

  const handleOrderDetailsInput = () => {
    PaymentModule.showOrderDetailsInput((err, input) => {
      setOverlayContent(input);
      setIsOverlayVisible(true);
    });
  };

  const handleOrderDetailsOutput = () => {
    if (!isSigned)
      PaymentModule.generateToast('Please sign first to see output');
    else {
      PaymentModule.showOrderDetailsOutput((err, output) => {
        setOverlayContent(output);
        setIsOverlayVisible(true);
      });
    }
  };

  const handleProcessInput = () => {
    PaymentModule.showProcessInput((err, input) => {
      setOverlayContent(input);
      setIsOverlayVisible(true);
    });
  };

  const handleProcessOutput = () => {
    if (!isSigned)
      PaymentModule.generateToast('Please process first to see output');
    else {
      PaymentModule.showProcessOutput((err, output) => {
        setOverlayContent(output);
        setIsOverlayVisible(true);
      });
    }
  };

  const handleSignOrderDetails = () => {
    PaymentModule.signOrderDetails();
    setIsSigned(true);
  };

  const handleProcess = () => PaymentModule.startJuspaySdk();

  const handleTerminate = () => PaymentModule.terminateJuspaySdk();

  return (
    <ScrollView>
      <Overlay isVisible={isOverlayVisible} height="auto">
        <View>
          <Text style={{marginBottom: 50, textAlign: 'justify'}}>
            {overlayContent}
          </Text>
          <Button
            title="Close"
            buttonStyle={styles.inputButton}
            onPress={handleOverlay}
          />
        </View>
      </Overlay>

      <Divider style={styles.divider} />
      <Button
        title="GENERATE ORDER ID"
        buttonStyle={styles.button}
        onPress={handleGenerateOrderId}
      />
      <Divider style={styles.divider} />
      <View style={styles.inputOutputGroup}>
        <Button
          title="INPUT"
          buttonStyle={styles.inputButton}
          onPress={handleOrderDetailsInput}
        />
        <Button
          title="OUTPUT"
          buttonStyle={styles.inputButton}
          onPress={handleOrderDetailsOutput}
        />
      </View>
      <Button
        title="SIGN ORDER DETAILS PAYLOAD"
        buttonStyle={styles.button}
        onPress={handleSignOrderDetails}
      />
      <Divider style={styles.divider} />

      <View style={styles.inputOutputGroup}>
        <Button
          title="INPUT"
          buttonStyle={styles.inputButton}
          onPress={handleProcessInput}
        />
        <Button
          title="OUTPUT"
          buttonStyle={styles.inputButton}
          onPress={handleProcessOutput}
        />
      </View>
      <Button
        title="PROCESS"
        buttonStyle={styles.button}
        onPress={handleProcess}
      />
      <Divider style={styles.divider} />

      <Button
        title="TERMINATE"
        buttonStyle={styles.button}
        onPress={handleTerminate}
      />
      <Divider style={styles.divider} />

      <Card style={styles.helpText}>
        <Text>
          Hit INPUT button to refer the given input AND OUTPUT button to refer
          output
        </Text>
        <Text>Step 1: Hit GENERATE ORDER ID button</Text>
        <Text>Step 2: Hit SIGN ORDER DETAILS PAYLOAD button</Text>
        <Text>Step 3: Hit PROCESS button</Text>
      </Card>

      <Divider style={styles.divider} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  divider: {
    backgroundColor: '#45A4A4',
    marginTop: 30,
    marginBottom: 30,
    height: 1,
  },
  inputButton: {
    backgroundColor: '#45A4A4',
    width: 150,
    height: 40,
    marginLeft: 20,
  },
  button: {
    backgroundColor: '#45A4A4',
    width: 350,
    height: 40,
    marginLeft: 20,
    marginTop: 10,
  },
  helpText: {
    color: '#45A4A4',
  },
  inputOutputGroup: {
    flexDirection: 'row',
  },
});

export default Process;
