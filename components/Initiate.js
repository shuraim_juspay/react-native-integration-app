import React, {useState, useEffect} from 'react';
import {ScrollView, View, StyleSheet} from 'react-native';
import {Button, Divider, Text, Card, Overlay} from 'react-native-elements';
import PaymentModule from './PaymentModule';

const Initiate = ({navigation}) => {

  useEffect(() => PaymentModule.clearVariables(),[]);

  const [isSigned, setIsSigned] = useState(false);
  const [isInitiated, setIsInitiated] = useState(false);

  const [isOverlayVisible, setIsOverlayVisible] = useState(false);
  const [overlayContent, setOverlayContent] = useState('');

  const handleOverlay = () => setIsOverlayVisible(false);

  const handleSignatureInput = () => {
    PaymentModule.showSignatureInput((err, input) => {
      setOverlayContent(input);
      setIsOverlayVisible(true);
    });
  };

  const handleSignatureOutput = () => {
    if (!isSigned)
      PaymentModule.generateToast('Please sign first to see output');
    else {
      PaymentModule.showSignatureOutput((err, output) => {
        setOverlayContent(output);
        setIsOverlayVisible(true);
      });
    }
  };

  const handleInitiateInput = () => {
    PaymentModule.showInitiateInput((err, input) => {
      setOverlayContent(input);
      setIsOverlayVisible(true);
    });
  };

  const handleInitiateOutput = () => {
    if (!isSigned)
      PaymentModule.generateToast('Please sign first to see output');
    else {
      PaymentModule.showInitiateOutput((err, output) => {
        setOverlayContent(output);
        setIsOverlayVisible(true);
      });
    }
  };

  const handleSignature = () => {
    PaymentModule.signSingaturePayload();
    setIsSigned(true);
  };

  const handleInitiate = () => {
    PaymentModule.initiateJuspaySdk((err, isInitiateComplete) => {
      if(isInitiateComplete)
      setIsInitiated(true);
    });
  };

  const handleTerminate = () => PaymentModule.terminateJuspaySdk();

  const handleProceed = () => {
    if (!isInitiated)
      PaymentModule.generateToast('Please initiate first to proceed');
    else navigation.navigate('Process');
  };

  return (
    <ScrollView>
      <Overlay isVisible={isOverlayVisible} height="auto">
        <View>
          <Text style={{marginBottom: 50, textAlign: 'justify'}}>
            {overlayContent}
          </Text>
          <Button
            title="Close"
            buttonStyle={styles.inputButton}
            onPress={handleOverlay}
          />
        </View>
      </Overlay>
      <Divider style={styles.divider} />
      <View style={styles.inputOutputGroup}>
        <Button
          title="INPUT"
          buttonStyle={styles.inputButton}
          onPress={handleSignatureInput}
        />
        <Button
          title="OUTPUT"
          buttonStyle={styles.inputButton}
          onPress={handleSignatureOutput}
        />
      </View>
      <Button
        title="SIGN CUSTOMER DETAILS PAYLOAD"
        buttonStyle={styles.button}
        onPress={handleSignature}
      />
      <Divider style={styles.divider} />

      <View style={styles.inputOutputGroup}>
        <Button
          title="INPUT"
          buttonStyle={styles.inputButton}
          onPress={handleInitiateInput}
        />
        <Button
          title="OUTPUT"
          buttonStyle={styles.inputButton}
          onPress={handleInitiateOutput}
        />
      </View>
      <Button
        title="INITIATE"
        buttonStyle={styles.button}
        onPress={handleInitiate}
      />
      <Divider style={styles.divider} />

      <Button
        title="TERMINATE"
        buttonStyle={styles.button}
        onPress={handleTerminate}
      />
      <Divider style={styles.divider} />

      <Card style={styles.helpText}>
        <Text>
          Hit INPUT button to refer the given input AND OUTPUT button to refer
          output
        </Text>
        <Text>Step 1: Hit SIGN CUSTOMER DETAILS PAYLOAD button</Text>
        <Text>Step 2: Hit INITIATE button</Text>
        <Text>Step 3: Hit PROCEED button</Text>
      </Card>

      <Divider style={styles.divider} />

      <Button
        title="PROCEED"
        buttonStyle={styles.button}
        onPress={handleProceed}
      />

      <Divider style={styles.divider} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  divider: {
    backgroundColor: '#45A4A4',
    marginTop: 30,
    marginBottom: 30,
    height: 1,
  },
  inputButton: {
    backgroundColor: '#45A4A4',
    width: 150,
    height: 40,
    marginLeft: 20,
  },
  button: {
    backgroundColor: '#45A4A4',
    width: 350,
    height: 40,
    marginLeft: 20,
    marginTop: 10,
  },
  helpText: {
    color: '#45A4A4',
  },
  inputOutputGroup: {
    flexDirection: 'row',
  },
});

export default Initiate;
